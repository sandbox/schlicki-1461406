<?php

/**
 * Load restrictions
 *
 * @param id (optional) The ID if the set.
 */
function node_ip_restriction_load($id = null) {
	$query = db_select('oc_node_ip_restriction_sets', 'r')
		->fields('r');

	if($id) {
		$entry = $query->condition('id', $id)
			->execute()
			->fetchAssoc();

		return $entry['ips'];
	}

	$result = $query->execute();

	$restrictions = array();
	while($entry = $result->fetchAssoc()) {
		$restrictions[] = $entry;
	}

	return $restrictions;
}

/**
 * Load restrictions for select field. This is used as the 'allowed_values_function' for field 'field_node_ip_restriction_sets'.
 */
function node_ip_restriction_select_options() {
	$result = db_select('oc_node_ip_restriction_sets', 'r')
		->fields('r')
		->execute();

	$restrictions = array();
	while($entry = $result->fetchAssoc()) {
		$restrictions[$entry['id']] = $entry['name'];
	}

	return $restrictions;
}

/**
 * Get allowed IPs for a node.
 *
 * @param $node The node object.
 */
function node_ip_resctriction_get_node_ips($node) {
	$node_ips = array();

	$items = field_get_items('node', $node, 'field_node_ip_restriction');
	if($items) {
		$value = field_view_value('node', $node, 'field_node_ip_restriction', $items[0]);
		foreach(preg_split('/\r\n|\r|\n/', $value['#markup']) as $ip) {
			$node_ips[] = $ip;
		}
	}

	$items = field_get_items('node', $node, 'field_node_ip_restriction_sets');
	if($items) {
		foreach($items as $item) {
			$value = field_view_value('node', $node, 'field_node_ip_restriction', $item);
			foreach(preg_split('/\r\n|\r|\n/', node_ip_restriction_load($value['#markup'])) as $ip) {
				$node_ips[] = $ip;
			}
		}
	}

	return $node_ips;
}